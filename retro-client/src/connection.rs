use std::net::{SocketAddr, TcpStream};
use std::path::Path;

use retro_common::*;

use serde_json;

use failure::*;

#[derive(Clone, Copy, Debug)]
enum ConnectionState {
    Connected,
    Handshaked,
    CoreLoaded,
}

pub struct Connection {
    sock: TcpStream,
    state: ConnectionState,
    /// Contains the supported file extensions
    capabilities_merged: String,
    capabilities: Vec<String>,
}

#[derive(Debug, Fail)]
enum ConnectionError {
    #[fail(
        display = "invalid state transition. Expected `{:?}', but `{:?}'",
        expected, actual
    )]
    WrongState {
        expected: ConnectionState,
        actual: ConnectionState,
    },
    #[fail(display = "invalid file extension `{:?}'", actual)]
    InvalidExtension { actual: Option<String> },
}

macro_rules! check_state {
    ($self:ident, $exp:path) => {
        match $self.state {
            $exp => Ok(()),
            _ => Err(ConnectionError::WrongState {
                actual: $self.state,
                expected: $exp,
            }),
        }
    };
}

impl Connection {
    pub fn new(addr: SocketAddr) -> Result<Connection, Error> {
        let stream = TcpStream::connect(addr)?;

        Ok(Connection {
            sock: stream,
            state: ConnectionState::Connected,
            capabilities: Vec::new(),
            capabilities_merged: String::new(),
        })
    }

    pub fn capabilities(&mut self) -> &[String] {
        match self.state {
            ConnectionState::Connected => {
                let adv: Advertize<String> = serde_json::Deserializer::from_reader(&mut self.sock)
                    .into_iter()
                    .next()
                    .unwrap()
                    .unwrap();

                self.capabilities_merged = adv.exts.join("|");
                self.capabilities = adv.exts;
                self.state = ConnectionState::Handshaked;
            }
            _ => (),
        };

        &self.capabilities
    }

    pub fn load_game(&mut self, path: &Path) -> Result<CoreStarted, Error> {
        check_state!(self, ConnectionState::Handshaked)?;

        let ext = path
            .extension()
            .ok_or(ConnectionError::InvalidExtension { actual: None })?
            .to_str()
            .unwrap()
            .to_string();

        if !self.capabilities.contains(&ext) {
            Err(ConnectionError::InvalidExtension {
                actual: Some(ext.clone()),
            })?
        }

        let path = path.to_str().unwrap().to_string();

        let msg = StartCore { ext, path };

        serde_json::to_writer(&mut self.sock, &msg)?;

        self.state = ConnectionState::CoreLoaded;

        Ok(serde_json::Deserializer::from_reader(&mut self.sock)
            .into_iter()
            .next()
            .unwrap()
            .unwrap())
    }

    pub fn request_frame(&mut self) -> Result<Frame<Vec<u8>, Vec<i16>>, Error> {
        check_state!(self, ConnectionState::CoreLoaded)?;

        serde_json::to_writer(&mut self.sock, &OnRun {phantom: 1})?;

        Ok(bincode::deserialize_from(&mut self.sock)
            .into_iter()
            .next()
            .unwrap())
    }
}
