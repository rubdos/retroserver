use std::env;
use std::ffi::CString;

use env_logger;
use libretro_backend::*;
use libretro_sys::{self, LogPrintfFn};
use log::{self, Log, Metadata, Record};

struct Logger {
    retro_logger: LogPrintfFn,
}

impl Log for Logger {
    fn enabled(&self, _meta: &Metadata<'_>) -> bool {
        true
    }

    fn log(&self, rec: &Record<'_>) {
        use libretro_sys::LogLevel;
        use log::Level::*;
        let level = match rec.level() {
            Error => LogLevel::Error,
            Warn => LogLevel::Warn,
            Info => LogLevel::Info,
            Debug => LogLevel::Debug,
            Trace => LogLevel::Debug, // libretro doesn't understand trace.
        };

        let text = format!("{}\r\n", rec.args());
        let text = CString::new(text).expect("libretro cannot log UTF-8");

        unsafe { (self.retro_logger)(level, text.as_ptr()) };
    }

    fn flush(&self) {
        //noop
    }
}

pub fn init(cb: &EnvCallback) {
    // Try initializing logger to the libretro logger.
    let iface = std::mem::MaybeUninit::<libretro_sys::LogCallback>::uninit();
    let r = unsafe {
        cb.call_environment(libretro_sys::ENVIRONMENT_GET_LOG_INTERFACE, &iface)
    }.is_ok();

    let l: Box<dyn Log> = if r {
        println!("Setting logger to retro");

        // If `r`, we assume the logger interface has been initialized.
        let iface = unsafe { iface.assume_init() };

        Box::new(Logger {
            retro_logger: iface.log,
        })
    } else {
        println!("Setting logger to env");
        let mut builder = env_logger::Builder::new();

        if let Ok(s) = env::var("RUST_LOG") {
            builder.parse_filters(&s);
        }

        Box::new(builder.build())
    };

    match log::set_boxed_logger(l) {
        Ok(_) => info!("Logger set"),
        Err(_) => error!("Logger already set"),
    }

    log::set_max_level(log::LevelFilter::Trace);

    info!("Logging init done");
}
