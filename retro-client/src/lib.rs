use failure;

use libretro_sys;
#[macro_use]
extern crate log;

use std::path::Path;
use std::sync::Mutex;

use libretro_backend::*;
// use libretro_sys::ENVIRONMENT_SET_SUPPORT_NO_GAME;

use failure::{Error, bail};

mod connection;
mod logger;

use self::connection::*;

struct RetroClient {
    connection: Option<Connection>,
}

const DEFAULT_CONFIG: &str = include_str!("default-config.toml");

lazy_static::lazy_static! {
    static ref ENVIRONMENT_CB: Mutex<Option<EnvCallback>> = { Mutex::new(None) };
    static ref SETTINGS: config::Config = {
        use config::{Config, File, FileFormat};

        let mut settings = Config::default();

        for path in &[
            "/etc/retro-client",
            "retro-client"
        ]{
            settings.merge(File::with_name(path).required(false))
                .expect("Config merge error with retro-client");
        }

        if settings.get_table("server").is_err() {
            settings.merge(File::from_str(DEFAULT_CONFIG, FileFormat::Toml))
                .expect("load default config");
        }

        settings
    };
}

impl Default for RetroClient {
    fn default() -> RetroClient {
        // Initialise logging
        logger::init(ENVIRONMENT_CB.lock().unwrap().as_ref().unwrap());

        RetroClient {
            connection: None,
        }
    }
}

fn capabilities() -> String {
    let servers = SETTINGS.get_table("server").unwrap();
    println!("servers in config: {:?}", servers);

    servers.into_iter().flat_map(|server| {
        let server = server.1.into_table().unwrap();
        let capabilities = server.get("capabilities").unwrap().clone()
            .into_array().unwrap();
        capabilities.into_iter().map(|a| a.into_str().unwrap())
    }).collect::<Vec<String>>().join(",")
}

impl RetroClient {
    fn connect_by_capability(&mut self, capability: &str) -> Result<(), Error> {
        debug!("Finding applicable server with {} capability", capability);

        let servers = SETTINGS.get_table("server").unwrap();
        for (name, server) in servers {
            let server = server.into_table()?;
            let mut capabilities = server.get("capabilities").unwrap().clone()
                .into_array()?
                .into_iter().map(|c| c.into_str().unwrap());

            if capabilities.find(|c| c == capability).is_some() {
                let addr = server.get("address").unwrap().clone().into_str()?;

                info!("Server `{}' ({}) is `{}'-capable", name, addr, capability);

                match Connection::new(addr.parse()?) {
                    Ok(mut connection) => {
                        // Now test whether server has this capability as advertised.
                        let caps = connection.capabilities();
                        info!("capabilities: {:?}", caps);

                        // XXX: this in unbelievably ugly.
                        if ! caps.contains(&capability.to_string()) {
                            continue;
                        }

                        self.connection = Some(connection);
                        return Ok(())
                    },
                    Err(e) => warn!("Cannot connect to {} ({}). Continuing", name, e),
                }
            }
        }

        bail!("No server with {} capability", capability);
    }
}

impl Core for RetroClient {
    fn info() -> CoreInfo {
        info!("info()");

        CoreInfo::new("RetroClient", env!("CARGO_PKG_VERSION"))
            .supports_roms_with_extension(&capabilities())
            .requires_path_when_loading_roms()
    }

    fn on_load_game(&mut self, d: GameData) -> LoadGameResult {
        info!("on_load_game(). Data: {:?}; path: {:?}", d.data(), d.path());

        let path = if let Some(path) = d.path() {
            path
        } else {
            error!("Cannot load game without path");
            return LoadGameResult::Failed(d);
        };

        let path = Path::new(path);

        let ext = path
            .extension()
            .unwrap() // XXX
            .to_str()
            .unwrap();

        if let Err(e) = self.connect_by_capability(ext) {
            error!("Could not connect for `{}': {}", ext, e);
            return LoadGameResult::Failed(d);
        }

        match self.connection.as_mut().expect("connection").load_game(&path) {
            Ok(started) =>  {
                info!("{:?}", started);
                LoadGameResult::Success(AudioVideoInfo::new()
                    .video(
                        started.base_width,
                        started.base_height,
                        started.fps,
                        libretro_sys::PixelFormat::from_uint(started.pixel_format).unwrap())
                    .max_video_size(started.max_width, started.max_height)
                    .aspect_ratio(started.aspect_ratio)
                    .audio(started.audio_sample_rate))
            },
            Err(e) => {
                error!("Could not load game {:?}", e);
                LoadGameResult::Failed(d)
            },
        }
    }

    fn on_unload_game(&mut self) -> GameData {
        unimplemented!();
    }

    fn on_run(&mut self, h: &mut RuntimeHandle) {
        info!("on run");
        match self.connection.as_mut().expect("connection").request_frame() {
            Ok(frame) => {
                info!("received a frame");
                h.upload_audio_frame(&frame.audio_data);
                h.upload_video_frame(&frame.data);
            },
            Err(e) => {
                error!("Could not request frame {:?}", e);
            }
        }
    }

    fn on_reset(&mut self) {
        unimplemented!();
    }

    fn on_set_environment(cb: EnvCallback) {
        println!("on-set-env");
        *ENVIRONMENT_CB.lock().unwrap() = Some(cb);
        // unsafe { cb.call_environment(ENVIRONMENT_SET_SUPPORT_NO_GAME, &true).unwrap(); }
    }
}

libretro_core!(RetroClient);
