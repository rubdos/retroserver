use std::collections::HashMap;
use std::net::{SocketAddr, TcpListener, TcpStream};
use std::rc::Rc;

use log::{info, warn};

use crate::core::*;

use retro_common::*;

pub struct Ready {
    cores: HashMap<&'static str, Vec<Rc<Core>>>,
}

pub struct Listening {
    cores: HashMap<&'static str, Vec<Rc<Core>>>,
    listener: TcpListener,
}

pub struct Accepted {
    core: LoadedCore,
    socket: TcpStream,
}

pub struct GameServer<State> {
    state: State,
}

impl GameServer<Ready> {
    pub fn new(loader: impl Iterator<Item = CoreLoader>) -> GameServer<Ready> {
        let mut cores: HashMap<&str, Vec<_>> = HashMap::new();

        for core in loader {
            let core = core.load().unwrap();
            info!("detected {}", core);

            let core = Rc::new(core);

            for ext in &core.supported_romfile_extensions {
                cores.entry(ext).or_default().push(core.clone())
            }
        }

        for (k, v) in &cores {
            if v.len() > 1 {
                warn!("{} has multiple emulators", k);
            }
        }

        info!("{} extensions supported", cores.len());

        GameServer {
            state: Ready { cores },
        }
    }

    pub fn bind(self, addr: SocketAddr) -> GameServer<Listening> {
        let listener = TcpListener::bind(&addr).unwrap();
        self.listen(listener)
    }

    pub fn listen(self, listener: ::std::net::TcpListener) -> GameServer<Listening> {
        let GameServer {
            state: Ready { cores },
        } = self;

        info!("Listening");

        GameServer {
            state: Listening { cores, listener },
        }
    }
}

impl GameServer<Listening> {
    pub fn accept_one(self) -> GameServer<Accepted> {
        let GameServer {
            state: Listening { cores, listener },
        } = self;

        let mut socket = listener.incoming().next().unwrap().unwrap();

        let adv = Advertize {
            exts: cores.keys().collect(),
        };

        ::serde_json::to_writer(&mut socket, &adv).unwrap();
        let start: StartCore<String> = ::serde_json::Deserializer::from_reader(&mut socket)
            .into_iter()
            .next()
            .unwrap()
            .unwrap();

        info!("request: {:?}", start);

        let core = cores.get::<str>(&start.ext).unwrap()[0].clone();
        drop(cores);
        let core = match Rc::try_unwrap(core) {
            Ok(c) => c,
            Err(_) => unreachable!(),
        };

        let mut core = core.initialize();
        core.load_game(start.path).unwrap();

        info!("{}", core);

        let av_info = core.get_system_av_info().unwrap();

        let loaded = CoreStarted {
            pixel_format: crate::core::ENVIRONMENT.lock().unwrap().pixel_format as u32,
            base_width: av_info.geometry.base_width,
            base_height: av_info.geometry.base_height,
            max_width: av_info.geometry.max_width,
            max_height: av_info.geometry.max_height,
            aspect_ratio: av_info.geometry.aspect_ratio,
            fps: av_info.timing.fps,
            audio_sample_rate: av_info.timing.sample_rate,
        };

        ::serde_json::to_writer(&mut socket, &loaded).unwrap();

        GameServer {
            state: Accepted { core, socket },
        }
    }
}

impl GameServer<Accepted> {
    pub fn run(self) {
        let GameServer {
            state: Accepted { mut core, mut socket },
        } = self;

        loop {
            let _phantom: OnRun = ::serde_json::Deserializer::from_reader(&mut socket)
                .into_iter().next().unwrap().unwrap();

            bincode::serialize_into(&mut socket, &mut core.run()).unwrap();
        }
    }
}
