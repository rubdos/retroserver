use std::ffi::{CString, CStr};
use std::collections::HashMap;

use failure::{bail, Error};

use libc;
use libretro_sys::LogLevel;

use log::{trace, debug, info, warn, error};

use crate::core::hw_render::{self, HwRenderBackend};

pub struct Variable {
    pub definition: String,
    pub options: Vec<String>,
    pub selection: CString,
}

impl Variable {
    fn from_system(definition: &CStr) -> Variable {
        let definition = definition.to_string_lossy().into_owned();

        let mut split = definition.split(";");
        let definition = split.next().unwrap().into();
        let options = split.next().unwrap();
        let options: Vec<String> = options.trim().split("|").map(|a| a.to_owned()).collect();

        let selection = CString::new(options[0].as_bytes().to_vec()).unwrap();

        Variable {
            definition,
            options,
            selection,
        }
    }
}

pub struct Environment {
    pub variables: HashMap<CString, Variable>,
    pub pixel_format: libretro_sys::PixelFormat,

    pub render_backend: Option<Box<dyn HwRenderBackend + Send + Sync>>,
    variables_updated: bool,
}

impl Default for Environment {
    fn default() -> Environment {
        Environment {
            variables: Default::default(),
            pixel_format: libretro_sys::PixelFormat::RGB565,

            render_backend: None,
            variables_updated: false,
        }
    }
}

unsafe extern "C" fn log_cb(level: LogLevel, fmt: *const libc::c_char) {
    use self::LogLevel::*;
    let fmt = format!("[core] {}", CStr::from_ptr(fmt).to_str().unwrap());
    match level {
        Debug => debug!("{}", fmt),
        Info => info!("{}", fmt),
        Warn => warn!("{}", fmt),
        Error => error!("{}", fmt),
    }
}

macro_rules! define_parameters {
    ($($param:ident => $type:ty $(: $extra:ident)?,)*) => {
        macro_rules! get_param {
            $(($param, $data:expr) => {
                define_parameters!($($extra)? IMPL, $data, $type)
            });*
        }
    };
    (IMPL, $data:expr, $type:ty) => {{
        #[allow(unused_mut)]
        let data: &mut $type = unsafe {
            std::mem::transmute($data)
        };
        data
    }};
    (CONST IMPL, $data:expr, $type:ty) => {{
        #[allow(unused_mut)]
        let data: &$type = unsafe {
            std::mem::transmute($data)
        };
        data
    }};
    (PTR IMPL, $data:expr, $type:ty) => {{
        #[allow(unused_mut)]
        let data: *mut $type = unsafe {
            std::mem::transmute($data)
        };
        data
    }};
}

define_parameters! {
    ENVIRONMENT_SET_VARIABLES => libretro_sys::Variable: PTR,
    ENVIRONMENT_GET_VARIABLE => libretro_sys::Variable,
    ENVIRONMENT_GET_LOG_INTERFACE => libretro_sys::LogCallback,
    ENVIRONMENT_SET_PERFORMANCE_LEVEL => libc::c_uint,
    ENVIRONMENT_GET_CAN_DUPE => bool,
    ENVIRONMENT_SET_PIXEL_FORMAT => libretro_sys::PixelFormat,
    ENVIRONMENT_SET_INPUT_DESCRIPTORS => libretro_sys::InputDescriptor: PTR,
    ENVIRONMENT_GET_SYSTEM_DIRECTORY => *mut libc::c_char: PTR,
    ENVIRONMENT_GET_VARIABLE_UPDATE => bool,
    ENVIRONMENT_SET_MEMORY_MAPS => libretro_sys::MemoryMap,
    ENVIRONMENT_SET_HW_RENDER => libretro_sys::HwRenderCallback,
    // ENVIRONMENT_SET_SUPPORT_ACHIEVEMENTS => bool: CONST,
}

impl Environment {
    pub fn call(&mut self, cmd: libc::c_uint, data: *mut libc::c_void) -> bool {
        use ::libretro_sys::*;
        macro_rules! match_cmd {
            ($($pattern:ident($binding:pat) => $code:block,)*) => {
                match cmd {
                    $($pattern => {
                        #[allow(unused_mut)]
                        let mut env_impl = || -> Result<(), Error> {
                            let $binding = get_param!($pattern, data);
                            $code
                        };
                        match env_impl() {
                            Ok(()) => return true,
                            Err(e) => {
                                warn!("Did not handle {}: {}", stringify!($pattern), e);
                                return false
                            },
                        }
                    },)*
                    _ => {
                        if (cmd & ENVIRONMENT_EXPERIMENTAL) != 0 {
                            warn!("unimplemented EXPERIMENTAL environment callback {}", cmd)
                        } else {
                            error!("unimplemented environment callback {}", cmd);
                        }

                        return false;
                    },
                }
            }
        }

        match_cmd! {
            ENVIRONMENT_SET_VARIABLES(mut variables) => {
                loop {
                    let variable = unsafe { &*variables };

                    if variable.key == std::ptr::null() || variable.value == std::ptr::null() {
                        break;
                    }

                    let key = unsafe { CStr::from_ptr(variable.key) };
                    let value = unsafe { CStr::from_ptr(variable.value) };
                    let var = self::Variable::from_system(value);

                    info!("env set var ({:?} -> {:?})", key, var.selection);
                    self.variables.insert(key.to_owned(), var);


                    variables = unsafe { variables.offset(1) };
                }
                Ok(())
            },
            ENVIRONMENT_GET_VARIABLE(variable) => {
                let key = unsafe { CStr::from_ptr(variable.key) };
                trace!("env get var ({:?})", key);
                match self.variables.get(key) {
                    Some(value) => {
                        let value: &CStr = value.selection.as_ref();
                        variable.value = value.as_ptr();
                    },
                    None => {
                        bail!("Unset env. var");
                    }
                }
                self.variables_updated = false;
                Ok(())
            },
            ENVIRONMENT_SET_PERFORMANCE_LEVEL(level) => {
                trace!("set performance level {}", level);
                Ok(())
            },
            ENVIRONMENT_GET_LOG_INTERFACE(data) => {
                trace!("env get log");
                data.log = log_cb;
                Ok(())
            },
            ENVIRONMENT_GET_CAN_DUPE(dupe) => {
                *dupe = true;
                Ok(())
            },
            ENVIRONMENT_SET_PIXEL_FORMAT(format) => {
                trace!("Set pixel format");
                self.pixel_format = *format;
                Ok(())
            },
            ENVIRONMENT_SET_INPUT_DESCRIPTORS(_descriptors) => {
                trace!("Set input descriptors");
                Ok(())
            },
            ENVIRONMENT_GET_SYSTEM_DIRECTORY(_ptr) => {
                bail!("Get system directory");
            },
            ENVIRONMENT_GET_VARIABLE_UPDATE(updated) => {
                *updated = self.variables_updated;
                Ok(())
            },
            ENVIRONMENT_SET_HW_RENDER(hw_render_cb) => {
                self.render_backend = Some(hw_render::new(hw_render_cb)?);
                hw_render_cb.get_current_framebuffer = get_current_framebuffer;
                hw_render_cb.get_proc_address = get_proc_address;
                Ok(())
            },
            ENVIRONMENT_SET_MEMORY_MAPS(_ptr) => {
                info!("Memory maps were exported from the core.");
                Ok(())
            },
            // ENVIRONMENT_SET_SUPPORT_ACHIEVEMENTS(support) => {
            //     if *support {
            //         info!("Core signifies support for archievements.");
            //     } else {
            //         info!("Core signifies no support for achievements.");
            //     }
            //     Ok(())
            // },
        }
    }
}

unsafe extern "C" fn get_current_framebuffer() -> libc::uintptr_t {
    0
}

unsafe extern "C" fn get_proc_address(_sym: *const libc::c_char) -> libretro_sys::ProcAddressFn {
    unimplemented!("get_proc_address")
}
