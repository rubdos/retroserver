use std::ffi::CStr;
use std::ops::Deref;
use std::path::PathBuf;

use failure::Error;
use libc;
use libloading::{Library, Symbol};
use libretro_sys::*;

pub struct CoreLoader(PathBuf);

impl CoreLoader {
    pub fn find() -> impl Iterator<Item = CoreLoader> {
        let path = "/usr/lib/libretro/";
        Self::find_at(path)
    }

    pub fn find_at(path: &str) -> impl Iterator<Item = CoreLoader> {
        ::glob::glob(&(path.to_owned() + "/*.so"))
            .expect("Failed to read")
            .filter_map(|p| p.map(CoreLoader).ok())
    }

    pub fn load(self) -> Result<crate::core::Core, Error> {
        let path = self.0;
        let lib = Library::new(&path)?;

        macro_rules! gen_api {
            ($($f:ident : $e:ty,)*$(,)*) => { CoreAPI {
                $($f: unsafe {
                    let tmp: Symbol<$e> = lib.get(stringify!($f).as_bytes())?;
                    *tmp.deref()
                }),*
            }}
        }

        // Literally from https://github.com/koute/libretro-sys/blob/master/src/lib.rs
        // Maybe we can even include_bytes! this thing.
        let api = gen_api! {
            retro_set_environment: unsafe extern "C" fn(callback: EnvironmentFn),
        retro_set_video_refresh: unsafe extern "C" fn(callback: VideoRefreshFn),
        retro_set_audio_sample: unsafe extern "C" fn(callback: AudioSampleFn),
        retro_set_audio_sample_batch: unsafe extern "C" fn(callback: AudioSampleBatchFn),
        retro_set_input_poll: unsafe extern "C" fn(callback: InputPollFn),
        retro_set_input_state: unsafe extern "C" fn(callback: InputStateFn),
        retro_init: unsafe extern "C" fn(),
        retro_deinit: unsafe extern "C" fn(),
        retro_api_version: unsafe extern "C" fn() -> libc::c_uint,
        retro_get_system_info: unsafe extern "C" fn(info: *mut SystemInfo),
        retro_get_system_av_info: unsafe extern "C" fn(info: *mut SystemAvInfo),
        retro_set_controller_port_device: unsafe extern "C" fn(port: libc::c_uint, device: libc::c_uint),
        retro_reset: unsafe extern "C" fn(),
        retro_run: unsafe extern "C" fn(),
        retro_serialize_size: unsafe extern "C" fn() -> libc::size_t,
        retro_serialize: unsafe extern "C" fn(data: *mut libc::c_void, size: libc::size_t),
        retro_unserialize: unsafe extern "C" fn(data: *const libc::c_void, size: libc::size_t) -> bool,
        retro_cheat_reset: unsafe extern "C" fn(),
        retro_cheat_set: unsafe extern "C" fn(index: libc::c_uint, enabled: bool, code: *const libc::c_char),
        retro_load_game: unsafe extern "C" fn(game: *const GameInfo) -> bool,
        retro_load_game_special: unsafe extern "C" fn(game_type: libc::c_uint, info: *const GameInfo, num_info: libc::size_t) -> bool,
        retro_unload_game: unsafe extern "C" fn(),
        retro_get_region: unsafe extern "C" fn() -> libc::c_uint,
        retro_get_memory_data: unsafe extern "C" fn(id: libc::c_uint) -> *mut libc::c_void,
        retro_get_memory_size: unsafe extern "C" fn(id: libc::c_uint) -> libc::size_t,
        };

        let (
            name,
            library_version,
            supported_romfile_extensions,
            require_path_when_loading_roms,
            allow_frontend_to_extract_archives,
        ) = unsafe {
            let mut system_info: SystemInfo = ::std::mem::zeroed();
            (api.retro_get_system_info)(&mut system_info);

            let name = CStr::from_ptr(system_info.library_name).to_str().unwrap();
            let library_version = CStr::from_ptr(system_info.library_version)
                .to_str()
                .unwrap();
            let supported_romfile_extensions = CStr::from_ptr(system_info.valid_extensions)
                .to_str()
                .unwrap();

            (
                name,
                library_version,
                supported_romfile_extensions,
                system_info.need_fullpath,
                system_info.block_extract,
            )
        };

        let supported_romfile_extensions = supported_romfile_extensions.split('|').collect();

        Ok(crate::core::Core {
            path,
            library: lib,

            library_name: name,
            library_version,
            supported_romfile_extensions,
            require_path_when_loading_roms,
            allow_frontend_to_extract_archives,

            api,
        })
    }
}
