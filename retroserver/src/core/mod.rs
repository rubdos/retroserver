use std::cell::RefCell;
use std::fmt::{self, Display, Formatter};
use std::path::PathBuf;
use std::sync::Mutex;
use std::ffi::CString;
use std::fs;

use log::{trace, info};
use libc;
use libloading::Library;
use libretro_sys::{CoreAPI, GameInfo};

use failure::{ensure, Error};

mod environment;
mod loader;
pub mod hw_render;

use self::environment::*;
pub use self::loader::*;

pub struct Core {
    path: PathBuf,
    library: Library,

    pub library_name: &'static str,
    pub library_version: &'static str,
    pub supported_romfile_extensions: Vec<&'static str>,
    pub require_path_when_loading_roms: bool,
    pub allow_frontend_to_extract_archives: bool,

    api: CoreAPI,
}

#[must_use = "Loaded core must call `deinitialize' after use"]
#[allow(dead_code)]
pub struct LoadedCore {
    path: PathBuf,
    library: Library,

    library_name: &'static str,
    library_version: &'static str,
    supported_romfile_extensions: Vec<&'static str>,
    require_path_when_loading_roms: bool,
    allow_frontend_to_extract_archives: bool,

    api: CoreAPI,
}

pub struct VideoBuffer {
    pub data: *const libc::c_void,
    pub width: libc::c_uint,
    pub height: libc::c_uint,
    pub pitch: libc::size_t,
}

impl Default for VideoBuffer {
    fn default() -> VideoBuffer {
        VideoBuffer {
            data: std::ptr::null(),
            width: 0,
            height: 0,
            pitch: 0,
        }
    }
}

impl VideoBuffer {
    pub fn encode_frame(
        &self,
        pixel_format: libretro_sys::PixelFormat,
    ) -> Result<Vec<u8>, Error> {
        info!("Encoding ({}x{}) with pixel format {:?}",
            self.width, self.height, pixel_format);

        // bytes per pixel
        use libretro_sys::PixelFormat::*;
        let bpp = match pixel_format {
            ARGB8888 => 4*8,
            ARGB1555 => 1 + 3*5,
            RGB565 => 5 + 6 + 5,
        } / 8;

        ensure!(self.pitch as u32 >= bpp * self.width, "Pitch cannot be smaller than bpp * width");
        let end_line = self.pitch as u32 - bpp* self.width;
        info!("Garbage at scan line: {}", end_line);

        let mut pixels = Vec::with_capacity((self.height * self.width * bpp) as usize);

        for line in 0..self.height as usize {
            let offset = line * self.pitch;
            let line = unsafe { self.data.offset(offset as isize) as *const u8 };
            let line = unsafe {
                std::slice::from_raw_parts(line, (self.width * bpp) as usize)
            };
            pixels.extend_from_slice(line);
        }

        Ok(pixels)
    }
}

#[derive(Default)]
pub struct AudioBuffer {
    data: Vec<i16>,
}

unsafe impl Send for VideoBuffer {}
unsafe impl Sync for VideoBuffer {}

lazy_static::lazy_static! {
    pub static ref ENVIRONMENT: Mutex<Environment> = { Mutex::new(Environment::default()) };
    pub static ref VIDEO_BUFFER: Mutex<RefCell<VideoBuffer>> = {
        Mutex::new(RefCell::new(VideoBuffer::default()))
    };
    pub static ref AUDIO_BUFFER: Mutex<RefCell<AudioBuffer>> = {
        Mutex::new(RefCell::new(AudioBuffer::default()))
    };
}

unsafe extern "C" fn environment_cb(cmd: libc::c_uint, data: *mut libc::c_void) -> bool {
    ENVIRONMENT.lock().unwrap().call(cmd, data)
}

extern "C"
fn video_refresh(
    data: *const libc::c_void,
    width: libc::c_uint,
    height: libc::c_uint,
    pitch: libc::size_t)
{
    trace!("video_refresh");
    *VIDEO_BUFFER.lock().unwrap().get_mut() = VideoBuffer {
        data, width, height, pitch,
    };
}

extern "C"
fn audio_sample_batch(data: *const i16,
                      frames: libc::size_t)
    -> libc::size_t
{
    trace!("audio sample batch, len {}", frames);
    // We cast this data to a 'static slice. We only use the slice for a single frame.
    AUDIO_BUFFER.lock().unwrap().get_mut()
        .data.extend_from_slice(unsafe {
            std::slice::from_raw_parts(data, frames * 2)
        });

    frames
}

extern "C"
fn input_poll()
{
    trace!("input_poll_fn");
}

extern "C"
fn input_state(_port: libc::c_uint,
               _device: libc::c_uint,
               _index: libc::c_uint,
               _id: libc::c_uint)
    -> i16
{
    0
}

impl Core {
    pub fn initialize(self) -> LoadedCore {
        let Core {
            path,
            library,

            library_name,
            library_version,
            supported_romfile_extensions,
            require_path_when_loading_roms,
            allow_frontend_to_extract_archives,

            api,
        } = self;

        // Set environment
        unsafe {
            (api.retro_set_environment)(environment_cb);
            (api.retro_init)();

            // Set other callbacks
            (api.retro_set_video_refresh)(video_refresh);
            (api.retro_set_input_poll)(input_poll);
            (api.retro_set_input_state)(input_state);
            (api.retro_set_audio_sample_batch)(audio_sample_batch);
        }

        LoadedCore {
            path,
            library,
            library_name,
            library_version,
            supported_romfile_extensions,
            require_path_when_loading_roms,
            allow_frontend_to_extract_archives,
            api,
        }
    }
}

impl LoadedCore {
    pub fn load_game(&mut self, path: String) -> Result<(), ()>{
        info!("load_game {:?}", path);

        let c_path = CString::new(path.clone().into_bytes()).unwrap().as_ptr();
        let info = if self.require_path_when_loading_roms {
            info!("Loading game with path {:?}", path);
            GameInfo {
                path: c_path,
                data: std::ptr::null(),
                size: 0,
                meta: std::ptr::null(),
            }
        } else {
            info!("Loading game with buffer {:?}", path);
            let buffer = fs::read(path).unwrap();
            let size = buffer.len();
            let data = buffer.into_boxed_slice();
            let data = Box::leak(data);
            // XXX: does C take ownership?
            GameInfo {
                path: c_path,
                data: data.as_ptr() as *const _,
                size,
                meta: std::ptr::null(),
            }
        };

        if unsafe { (self.api.retro_load_game)(&info as *const _) } {
            Ok(())
        } else {
            Err(())
        }
    }

    pub fn get_system_av_info(&mut self) -> Result<libretro_sys::SystemAvInfo, ()> {
        let mut av_info  = std::mem::MaybeUninit::<libretro_sys::SystemAvInfo>::uninit();
        let av_info = unsafe {
            (self.api.retro_get_system_av_info)(av_info.as_mut_ptr());
            av_info.assume_init()
        };

        Ok(av_info)
    }

    pub fn run<'s>(&'s mut self) -> retro_common::Frame<Vec<u8>, Vec<i16>> {
        trace!("run called");
        unsafe { (self.api.retro_run)() };
        let video = VIDEO_BUFFER.lock().unwrap();
        let video = video.borrow();
        let env = ENVIRONMENT.lock().unwrap();

        let video = video.encode_frame(
            env.pixel_format,
        ).unwrap();

        let mut audio = AUDIO_BUFFER.lock().unwrap();
        let audio = audio.get_mut();

        let empty = Vec::with_capacity(audio.data.len());
        let audio = std::mem::replace(&mut audio.data, empty);

        retro_common::Frame {
            data: video,
            audio_data: audio,
        }
    }

    #[allow(dead_code)]
    pub fn deinitialize(self) -> Core {
        let LoadedCore {
            path,
            library,

            library_name,
            library_version,
            supported_romfile_extensions,
            require_path_when_loading_roms,
            allow_frontend_to_extract_archives,

            api,
        } = self;

        // Set environment
        unsafe {
            (api.retro_deinit)();
        }

        Core {
            path,
            library,
            library_name,
            library_version,
            supported_romfile_extensions,
            require_path_when_loading_roms,
            allow_frontend_to_extract_archives,
            api,
        }
    }
}

impl Display for Core {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Core<{}, version {}, at {:?}>",
            self.library_name, self.library_version, self.path
        )
    }
}

impl Display for LoadedCore {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Core<{}, version {}, at {:?} --- Loaded>",
            self.library_name, self.library_version, self.path
        )
    }
}
