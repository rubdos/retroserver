use std::sync::Arc;
use libretro_sys::HwRenderCallback;
use failure::{bail, Error};
use crate::core::hw_render::HwRenderBackend;

use vulkano::instance::{Instance, InstanceExtensions};
use vulkano::app_info_from_cargo_toml;

#[derive(Debug)]
pub struct VkBackend {
    instance: Arc<Instance>,
}

impl HwRenderBackend for VkBackend {
    fn new(_hw_render_cb: &mut HwRenderCallback) -> Result<VkBackend, Error> {
        let app_info = app_info_from_cargo_toml!();
        let instance = {
            let extensions = InstanceExtensions::none();
            Instance::new(Some(&app_info), &extensions, None)?
        };

        Ok(VkBackend {
            instance,
        })
    }

    fn get_current_framebuffer(&self) -> libretro_sys::Framebuffer {
        unimplemented!("get_current_framebuffer")
    }
}
