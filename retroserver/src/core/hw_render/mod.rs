use failure::{bail, Error};

use libretro_sys::{HwContextType, HwRenderCallback};
use log::{info};

pub mod vk;

pub trait HwRenderBackend {
    fn new(hw_render_cb: &mut HwRenderCallback) -> Result<Self, Error>
        where Self: Sized;
    fn get_current_framebuffer(&self) -> libretro_sys::Framebuffer;
}

pub fn new(
    hw_render_cb: &mut HwRenderCallback
) -> Result<Box<dyn HwRenderBackend + Send + Sync>, Error> {
    let context_type = match HwContextType::from_uint(hw_render_cb.context_type) {
        Some(context_type) => context_type,
        None => bail!("Unknown HwContextType: {}", hw_render_cb.context_type),
    };

    match context_type {
        HwContextType::Vulkan => {
            info!("Creating Vulkan context");
            Ok(Box::new(vk::VkBackend::new(hw_render_cb)?))
        },
        _ => {
            bail!("Unimplemented hardware renderer {:?}", context_type);
        },
    }
}
