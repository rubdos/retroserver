use env_logger;
use log::info;

mod core;
mod gameserver;

use listenfd::ListenFd;

fn main() {
    env_logger::init();
    let loader = core::CoreLoader::find();
    let srv = gameserver::GameServer::new(loader);

    let mut manager = ListenFd::from_env();

    // if we are given a tcp listener on listen fd 0, we use that one
    let srv = if let Some(listener) = manager.take_tcp_listener(0).unwrap() {
        info!("Listening on provided socket");
        srv.listen(listener)
    // otherwise fall back to local listening
    } else {
        info!("Listening on new bound socket");
        srv.bind("[::]:3180".parse().unwrap())
    };

    let srv = srv.accept_one();
    srv.run();
}
