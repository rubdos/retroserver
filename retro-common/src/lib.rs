use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct Advertize<S> {
    pub exts: Vec<S>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct StartCore<S> {
    pub ext: S,
    pub path: S,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct CoreStarted {
    pub pixel_format: u32,
    pub base_width: u32,
    pub base_height: u32,
    pub max_width: u32,
    pub max_height: u32,
    pub aspect_ratio: f32,
    pub fps: f64,
    pub audio_sample_rate: f64,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OnRun {
    pub phantom: u8,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Frame<Data: AsRef<[u8]>, Audio: AsRef<[i16]>> {
    pub data: Data,
    pub audio_data: Audio,
}
